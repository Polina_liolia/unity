﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitBlock : MonoBehaviour {

    public GameObject prefab;
    public static float gridX = 5f;
    public static float gridY = 5f;

    public float spacing = 25f;

    void Start()
    {
        for(int y = 0; y < gridY; y++)
        {
            for (int x = 0; x< gridX; x++)
            {
                Vector3 pos = new Vector3(x, y, 0) * spacing;
                Instantiate(prefab, pos, Quaternion.identity);
            }
        }
    }
}
