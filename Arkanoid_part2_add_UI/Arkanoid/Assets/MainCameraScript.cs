﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraScript : MonoBehaviour {

    public static int blocks;
    public static int racket = 3;
    public int stepX = 10;
    public int W = 100;
    public int H = 25;
    public int maxWidth;
    public int maxHeight;
    public bool btnClicked;
    public bool checkResult;
    public int counter;
    public bool isWindowShow = true;
    void OnGUI()
    {
       maxWidth = Screen.width;
       maxHeight = Screen.height;
       //W = (maxWidth / 3) / 6;
       //H = (maxHeight / 3) / 12;
       GUI.Label(new Rect(maxWidth - stepX, 0, W, H),
           "очки " + blocks);
       //Box
       //Button
       //Toggle
       GUI.Box( new Rect(5,10,maxWidth / 3 - 10,maxHeight / 3 ),"Title" );
       GUI.BeginGroup(new Rect(5, 10, maxWidth / 3 - 10, maxHeight / 3));
       GUI.Label(new Rect(10, 10, 150, 25), "Кнопку нажали " + counter + " раз");
       btnClicked = GUI.Button(new Rect(10, 10 + 25, 100, 25),"Нажми меня!");
        if (btnClicked)
        {//OnClick
            counter++;
            
        }
        checkResult = GUI.Toggle(new Rect(10, 60, 125, 25),
            checkResult, 
            "show window #0 ?");
        if (checkResult)
        {
            isWindowShow = false;
        }
        else
        {
            isWindowShow = true;
        }
       GUI.EndGroup();

       Rect windowRect = new Rect(10, 
                                  maxHeight / 3 + 25, 
                                  maxWidth / 3, 
                                  maxHeight / 3);
        if (isWindowShow)
        {
            windowRect = GUI.Window(0,//номер этого окна
                windowRect,
                drawWindowContent,
                "Тестовое окно № 0"
                );
        }
    }
    private void drawWindowContent(int idWindow)
    {
        GUI.Label(new Rect(10, 0, 100, 25), "label 1");
        GUI.Label(new Rect(10, 25, 100, 25), "label 2");
        GUI.Label(new Rect(10, 50, 100, 25), "label 3");
        /*
        GUI.DragWindow( new Rect(0,
            0,
            10000,
            10000));
        */
    }

}
