﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using MyLibrary35;

public class CallRestService : MonoBehaviour {

	// Use this for initialization
	void Start () {
        
        StartCoroutine(getWWW());
        StartCoroutine(getUnityWebRequest());
    }

    #region GET
    private IEnumerator getWWW()
    {
        string url = "http://localhost:1497/api/fruits";
        WWW www = new WWW(url);
        while (!www.isDone)
            yield return null;
        if(string.IsNullOrEmpty(www.error))
        {
            Debug.Log(www.text);
        }
    }

    private IEnumerator getUnityWebRequest()
    {
        string url = "http://localhost:1497/api/fruits";
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.Send();
        if (www.isError)
        {
            Debug.Log(www.error + " error code: " + www.responseCode);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
        }
    }
    #endregion

    // Update is called once per frame
   
}
