﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Linq;
using System;

public class Recognition : MonoBehaviour {

    KeywordRecognizer keywordRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();

    private void Start()
    {
        Debug.Log("Start");
        keywords.Add("go", () =>
        {
            PrintWord("go");
        });
        keywords.Add("dog", () =>
        {
            PrintWord("dog");
        });
        keywords.Add("cat", () =>
        {
            PrintWord("cat");
        });
        keywords.Add("house", () =>
        {
            PrintWord("house");
        });
        keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizerOnPhraseRecognized;
       
    }

    private void KeywordRecognizerOnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        Debug.Log("KeywordRecognizerOnPhraseRecognized");
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }

    private void PrintWord(string word)
    {
        Debug.Log("You just said " + word);
    }
}
