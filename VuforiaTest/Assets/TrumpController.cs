﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class TrumpController : MonoBehaviour {

    private Animation anim;
    private Rigidbody rb;

    [SerializeField]
    private float speed = 4f;
	
    // Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animation>();
	}
	
	// Update is called once per frame
	void Update () {
        float x = CrossPlatformInputManager.GetAxis("Horizontal");
        float y = CrossPlatformInputManager.GetAxis("Vertical");

        Vector3 movement = new Vector3(x, 0, y);
        rb.velocity = movement * speed; //movement - 0;1

        if(x != 0 && y != 0) //trump is moving
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, //don't change
                                                Mathf.Atan2(x, y) * Mathf.Rad2Deg, //rotation in degrees
                                                transform.eulerAngles.z); //don't change
        }

        if(x != 0 || y != 0) //trump is moving
        {
            anim.Play("walk");
        }
        else
        {
            anim.Play("idle");
        }
	}
}
