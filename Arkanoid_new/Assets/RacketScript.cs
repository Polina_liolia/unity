﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacketScript : MonoBehaviour {

    public static float speed = 200;

    private void FixedUpdate() //после завершения цикла рассчета физики
    {
        float h = Input.GetAxisRaw("Horizontal"); //h<0 h>0
        GetComponent<Rigidbody2D>().velocity = h * speed * Vector2.right; //вектро скорости

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Vector3 position = this.transform.position;
            position.x -= h;
            this.transform.position = position;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            Vector3 position = this.transform.position;
            position.x = +h;
            this.transform.position = position;
        }

    }
}
