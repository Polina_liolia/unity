﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraScript : MonoBehaviour {
    public static int score;
    public static int racket;

	// Use this for initialization
	void Start () {
        score = 0;
        racket = 5;
	}

    private void OnGUI()
    {
        GUI.Label(new Rect(10, 20, 100, 25), string.Format("Score {0}", score));
        GUI.Label(new Rect(10, 45, 100, 25), string.Format("Helth {0}", racket));
    }

}
