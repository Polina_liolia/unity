﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockGreenScript : MonoBehaviour {

    private void OnCollisionEnter2D(Collision2D collision)
    {
       //gameObject - объкт, на ктором лежит скрипт (блок)
       //collision.gameObject - объект, с которым произошло столкновение(мяч)
       if (collision.gameObject.name == "ball")
        {
            MainCameraScript.score++;
            Destroy(gameObject);
        }
    }
}
