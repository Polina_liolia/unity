﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderBottomScript : MonoBehaviour {

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.name == "ball")
        {
            MainCameraScript.racket--;
            if(MainCameraScript.racket < 0)
            {
                Destroy(collision.gameObject);
            }

        }
    }
}
