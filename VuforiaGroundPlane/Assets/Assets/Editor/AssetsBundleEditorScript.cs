﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AssetsBundleEditorScript : MonoBehaviour {

    [MenuItem("Assets/Build AssetBundles")]
    static void BuildAllAssetBundles()
    {
        BuildPipeline.BuildAssetBundles("Assets/MyBundles", BuildAssetBundleOptions.None, BuildTarget.Android);
    }
}
